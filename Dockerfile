FROM openjdk:11-jre-slim

VOLUME /vol

ENV spring.profiles.active=mysql

ADD /target/*.jar petclinic.jar

CMD ["java","-jar","/petclinic.jar"]
